/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   inconsistencytable.h
 * Author: pabrod
 *
 * Created on June 13, 2017, 11:41 PM
 */
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

#include "readInfo.h"

#ifndef INCONSISTENCYTABLE_H
#define INCONSISTENCYTABLE_H

#ifdef __cplusplus
extern "C" {
#endif

    class inconsistencyTable{
    private:
        u_int64_t TableSize;
        Contig* contigTable[TableSize];
    public:
        inconsistencyTable(u_int64_t size);
        int computeHash(string id);
    
        void insertItem(string id, string taxonomy, contig* contigs);
        void removeItem(string id);

        int numberOfItemsInIndex(int index);

        void printTable();
        void printItemsInIndex(int index);

        int findInconsistencies();
    };


#ifdef __cplusplus
}
#endif

#endif /* INCONSISTENCYTABLE_H */

