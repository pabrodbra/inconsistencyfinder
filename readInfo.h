/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   readInfo.h
 * Author: pabrod
 *
 * Created on June 13, 2017, 11:31 PM
 */
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

#include "contigInfo.h"

#ifndef READINFO_H
#define READINFO_H

#ifdef __cplusplus
extern "C" {
#endif

    struct Read{
        string ID;
        string Taxonomy;
        Contig* contigs;
        Read* next;
    };



#ifdef __cplusplus
}
#endif

#endif /* READINFO_H */

