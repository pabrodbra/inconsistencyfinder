/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   contigInfo.h
 * Author: pabrod
 *
 * Created on June 13, 2017, 11:23 PM
 */
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

#ifndef CONTIGINFO_H
#define CONTIGINFO_H

#ifdef __cplusplus
extern "C" {
#endif

    struct Contig{
        string ID;
        string Taxonomy;
        Contig* next;
    };


#ifdef __cplusplus
}
#endif

#endif /* CONTIGINFO_H */

